package com.pricetag;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

public class RutenSearchThread extends Thread{

    private String mQuery;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "UTF-8";
    private int pLowerBound = 1000;
    private int pUpperBound = 5000;
    public RutenSearchThread(String query){
        mQuery = query;
    }

    @Override
    public void run(){
        rutenSearch();
    }

    public Set<Item> getItemSet(){
        return mItemSet;
    }

    // search ruten website
    private void rutenSearch(){
        String queryStrEnc = "";
        try{
            queryStrEnc = URLEncoder.encode(mQuery, M_CHARSET);
        }catch (Exception e){
        }
        String searchUrl = "http://m.ruten.com.tw/search/search.php?k=" + queryStrEnc;

        searchUrl += "&f=&p1=" + String.valueOf(pLowerBound) + "&p2=" + String.valueOf(pUpperBound);

        try {
            exeTimer.start("ruten download web");
            WebThread webThread = new WebThread(searchUrl, M_CHARSET);
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_ruten.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("ruten parse web ");
            Elements elements = doc.select("a.goods-item");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize()/1024 + " KB", "Ruten");
            Debugger.println("elementsCount = " + elements.size(), "Ruten");

            exeTimer.start("ruten match items");
            for (Element elem : elements) {
                String item_text = elem.select("div.item-name-text").text();
                int item_price;
                try{
                    item_price  = Integer.parseInt(elem.select("div.item-price").text());
                }catch (NumberFormatException e){
                    continue; // skip this item
                }

                if(KeywordSplitter.stringMatchCount(mQuery, item_text) == 0){
                    continue; // skip this item
                }

                String imgUrl = elem.select("div.item-figure").select("img").attr("src");

                String pid = elem.select("a").attr("data-gno");
                String url = "http://goods.ruten.com.tw/item/show?" + pid;
                Item item = new Item("<露天拍賣>"+item_text, item_price, url, imgUrl);
                mItemSet.add(item);
            }
            exeTimer.stop();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
