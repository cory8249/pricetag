package com.pricetag;

public class Config {
    public enum Platform{PLATFORM_PC, PLATFORM_ANDROID}

    public static final String STORAGE_PATH = "./";
    // public static final String STORAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";


    public static final String LOG_FILENAME =  "/debug_msg.txt";

    public static boolean debuggerPrintEnabled = true;
    public static boolean debuggerLogFileEnabled = true;
}
