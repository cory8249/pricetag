package com.pricetag;

import java.io.*;

public class Debugger {

    private static File mLogFile;
    private static PrintWriter printWriter;
    private static final String M_CHARSET = "UTF-8";

    static {
        try {
            mLogFile = new File(Config.STORAGE_PATH + Config.LOG_FILENAME);
            printWriter = new PrintWriter(mLogFile, M_CHARSET);
        } catch (final IOException e) {
            throw new ExceptionInInitializerError(e.getMessage());
        }
    }

    public static void print(Object msg, String... tags){
        if(!Config.debuggerPrintEnabled)
            return;

        for(String tag : tags){
            System.out.print("[" + tag + "]");
        }
        if(tags.length != 0){
            System.out.print("  ");
        }
        System.out.print(msg.toString());
    }

    public static void println(Object msg, String... tags){
        if(!Config.debuggerPrintEnabled)
            return;
        print(msg.toString() + "\n", tags);
    }

    public static void printToFile(Object msg){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.print(msg.toString());
    }

    public static void printlnToFile(Object msg){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.print(msg.toString() + "\n");
    }

    public static void closeFile(){
        if(!Config.debuggerLogFileEnabled)
            return;
        printWriter.close();
    }
}
