package com.pricetag;

import java.util.ArrayList;
import java.util.List;

public class Clusters {
    private List<Item> itemList;
    private List<Integer> priceList;
    private List<Cluster> structure;
    private List<List<Integer>> distMatrix;
    private double BGSS;    //between-group sum of squares(variance)
    private double WGSS;    //within-group sum of squares(variance)
    private double VRC;     //         BGSS*(N-k)
                            // VRC = --------------
                            //         WGSS*(k-1)
    private double m;   //overall mean

    //constructor
    public Clusters(List<Item> data)
    {
        this.itemList = data;
        this.priceList = new ArrayList<>();
        this.structure = new ArrayList<>();

        for(Item item : data)
        {
            //if(!priceList.contains(item.getPrice()))
            {
                this.priceList.add(item.getPrice());
                //Every item is a cluster at the beginning.
                Cluster cluster = new Cluster(item.getPrice());
                this.structure.add(cluster);
            }
        }
        m = calMean(this.priceList);

        //construct the distance matrix
        this.distMatrix = new ArrayList<>();
        int metric = 0;
        for(int i = 0; i<structure.size(); ++i)
        {
            List<Integer> row = new ArrayList<>();
            for(int j = 0; j<structure.size(); ++j)
            {
                metric = structure.get(i).getCentroid()-structure.get(j).getCentroid();
                metric = Math.abs(metric);
                row.add(j, metric);
            }
            distMatrix.add(i,row);
        }
    }

    //Cluster the items based on their prices
    public void gen()
    {
        if(itemList.size()!=0)
        {
            System.out.println("Start clustering ...");
            System.out.println(this);

            while(structure.size()!=1)
            {
                List<Cluster> nearestCluster = findMinDist();
                if(nearestCluster.size()==2)
                {
                    //merge the two clusters with nearest distance
                    merge(nearestCluster);
                }
                System.out.println(this);

                //calculate VRC
                calBGSS();
                calWGSS();
                this.VRC = BGSS*(priceList.size()-structure.size())/(WGSS*(structure.size()));
                //this.VRC = BGSS/WGSS;
                //System.out.println(BGSS);
                //System.out.println(WGSS);
                System.out.println(this.VRC);
            }
        }
        else
        {
            System.out.println("No item for clustering :O");
        }
    }

    //Find the minimum distance among clusters
    private List<Cluster> findMinDist()
    {
        List<Cluster> nearestCluster = new ArrayList<>();
        int min,min_i,min_j;
        //Bug: IndexOutOfBoundsException when input item set is empty
        min = distMatrix.get(0).get(1);
        min_i = 0;
        min_j = 1;

        for(int i = 0; i<structure.size(); ++i)
        {
            for(int j = 0; j<structure.size(); ++j)
            {
                if(distMatrix.get(i).get(j)<min && i!=j)
                {
                    min = distMatrix.get(i).get(j);
                    min_i = i;
                    min_j = j;
                }
            }
        }

        nearestCluster.add(structure.get(min_i));
        nearestCluster.add(structure.get(min_j));
        return nearestCluster;
    }

    //Merge the nearest clusters and update the distance matrix
    private void merge(List<Cluster> nearestCluster)
    {
        Cluster major;
        Cluster minor;
        if(nearestCluster.get(0).getSize()>nearestCluster.get(1).getSize())
        {
            major = nearestCluster.get(0);
            minor = nearestCluster.get(1);
        }
        else
        {
            major = nearestCluster.get(1);
            minor = nearestCluster.get(0);
        }
        int minorIndex = structure.indexOf(minor);

        //insert members of the minor into the major cluster
        major.insert(minor.getMembers());

        //remove the minor cluster from structure
        structure.remove(minor);

        //update the distance matrix
        removeColumn(minorIndex);
        removeRow(minorIndex);
        List<Integer> distUpdates = new ArrayList<>();
        int distUpdate;
        int majorIndex = structure.indexOf(major);
        for(int i = 0; i<structure.size(); ++i)
        {
            distUpdate = major.getCentroid()-structure.get(i).getCentroid();
            distUpdate = Math.abs(distUpdate);
            distUpdates.add(distUpdate);
            //update column
            distMatrix.get(i).set(majorIndex,distUpdate);
        }
        //update row
        distMatrix.set(majorIndex, distUpdates);

    }

    private void removeColumn(int index)
    {
        for (List<Integer> row : distMatrix)
        {
            row.remove(index);
        }

    }

    private void removeRow(int index)
    {
        distMatrix.remove(index);
    }

    private Integer calMean(List<Integer> numbers)
    {
        int sum=0;
        for(Integer number : numbers){
            sum += number;
        }
        return sum/numbers.size();
    }

    //Calculate between-group sum of squares(variance)
    private void calBGSS()
    {
        this.BGSS = 0;
        double mi;
        int ni;
        for(Cluster cluster : structure)
        {
            mi = cluster.getCentroid();
            ni = cluster.getSize();
            this.BGSS += (ni*(m-mi)*(m-mi));
        }
    }

    //Calculate within-group sum of squares(variance)
    private void calWGSS()
    {
        this.WGSS = 0;
        for(Cluster cluster : structure)
        {
            this.WGSS += cluster.getWGSS();
        }
    }

    //Output the current clusters
    @Override
    public String toString()
    {
        String clusterToString = "";
        for(Cluster cluster : structure)
        {
            clusterToString = clusterToString + cluster.toString() + " ";
        }
        return clusterToString;
    }
}

