package com.pricetag;
import java.util.*;

public class KeywordSplitter {

    enum CharType {ENG, NUM, CHINESE, SYM}

    public static Set<String> splitToKeywords(String text){
        CharType[] charArray = analysisCharType(text);
        Set<String> keywordSet = new HashSet<>();
        List<Integer> splitPoints = new ArrayList<>();

        // split points
        splitPoints.add(0);
        for(int i=0; i<charArray.length-1; i++){
            if(charArray[i] != charArray[i+1]){
                splitPoints.add(i+1);
            }
        }
        splitPoints.add(charArray.length);

        String bufStr = null;
        for(int i=0; i<splitPoints.size()-1; i++){
            int begin = splitPoints.get(i);
            int end = splitPoints.get(i + 1);

            //System.out.print("(" + begin + "," + end + ") = ");
            String str = text.substring(begin, end);

            //System.out.println(str);

            // determine whether to split at this point
            if(charArray[begin] == CharType.SYM){
                if(bufStr != null) {
                    keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
                    bufStr = null;
                }
                continue;
            }else if((charArray[begin] == CharType.ENG || charArray[begin] == CharType.NUM)
                    && str.length() <= 4){
                // detect short eng & num, store to buffer
                if(bufStr != null){
                    bufStr += str;
                }else {
                    bufStr = str;
                }
                continue;
            }

            // normal case
            keywordSet.add(str.toLowerCase(Locale.ENGLISH));

            if(bufStr != null) {
                keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
                bufStr = null;
            }
            //System.out.println(end + ":" + str);
        }
        if(bufStr != null) {
            keywordSet.add(bufStr.toLowerCase(Locale.ENGLISH));
        }

        return keywordSet;
    }

    public static int stringMatchCount(String str1, String str2){
        Set<String> set1 = splitToKeywords(str1);
        String str2Lower = str2.toLowerCase(Locale.ENGLISH);
        int counter = 0;
        for(String s1 : set1){
            if(str2Lower.contains(s1.toLowerCase(Locale.ENGLISH))){
                counter++;
            }
        }
        return counter;
    }


    // determine character type (english, number, chinese, symbols)
    private static CharType[] analysisCharType(String str){
        CharType[] cType = new CharType[str.length()];
        for(int i=0; i<str.length(); i++){
            char c = str.charAt(i);
            int cp = str.codePointAt(i);
            if(c >= '0' && c<= '9'){
                cType[i] = CharType.NUM;
            }else if(c >= 'A' && c <= 'Z'){
                cType[i] = CharType.ENG;
            }else if(c >= 'a' && c <= 'z'){
                cType[i] = CharType.ENG;
            }else if (cp > 0x4E00 && cp < 0x9FCC) { // Chinese character unicode range
                cType[i] = CharType.CHINESE;
            }else if(cp == '+' || cp == '.'){
                cType[i] = CharType.ENG; // set '+' & '.' as a character
            }else{
                cType[i] = CharType.SYM;
            }
        }
        return cType;
    }
}
