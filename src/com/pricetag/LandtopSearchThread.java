package com.pricetag;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.PrintWriter;
import java.util.*;

public class LandtopSearchThread extends Thread {

    private String mQuery;
    private Set<String> mSearchSet;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "UTF-8";

    public LandtopSearchThread(String query) {
        mQuery = query;
        mSearchSet = KeywordSplitter.splitToKeywords(mQuery);
    }

    public Set<Item> getItemSet() {
        return mItemSet;
    }

    @Override
    public void run() {
        LandtopSearch();
    }

    // search from landtop website
    private void LandtopSearch() {

        String landtopUrl = "http://www.landtop.com.tw/products.php?types=1";
        Map<Item, Integer> candidates = new HashMap<>();
        List<Item> landtopItem = new ArrayList<>();

        try {
            exeTimer.start("landtop download web");
            WebThread webThread = new WebThread(landtopUrl, M_CHARSET);
            webThread.useCache(true);
            webThread.setCacheExpireTimeMs(60 * 60 * 1000L); // expiration time: 1 hour
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_landtop.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("landtop parse web");
            Elements elements = doc.select("tr");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Landtop");
            Debugger.println("elementsCount = " + elements.size(), "Landtop");

            exeTimer.start("Landtop match items");
            for (Element elem : elements) {
                String item_text = elem.select("h5 > a").text();
                int item_price;
                try{
                    item_price  = Integer.parseInt(elem.select("td.price").text().replace("$ ",""));
                }catch (NumberFormatException e){
                    continue; // skip this item
                }
                String url = elem.select("a").attr("href");

                if(url.contains("idept=1&")){
                    item_text = "apple 蘋果 " + item_text;
                }else if(url.contains("idept=2&")){
                    item_text = "asus 華碩 " + item_text;
                }else if(url.contains("idept=7&")){
                    item_text = "htc " + item_text;
                }else if(url.contains("idept=4&")){
                    item_text = "lg " + item_text;
                }else if(url.contains("idept=6&")){
                    item_text = "samsung 三星 " + item_text;
                }else if(url.contains("idept=5&")){
                    item_text = "sony " + item_text;
                }else if(url.contains("idept=9&")){
                    item_text = "infocus " + item_text;
                }else if(url.contains("idept=14&")){
                    item_text = "huawei 華為 " + item_text;
                }else if(url.contains("idept=13&")){
                    item_text = "xiaomi 小米 " + item_text;
                }else if(url.contains("idept=20&")){
                    item_text = "oppo " + item_text;
                }else if(url.contains("idept=16&")){
                    item_text = "benten " + item_text;
                }else if(url.contains("idept=17&")){
                    item_text = "nokia " + item_text;
                }else if(url.contains("idept=25&")){
                    item_text = "hugiga " + item_text;
                }


                Item item = new Item(item_text, item_price, "http://www.landtop.com.tw/"+url, "");
                landtopItem.add(item);

                // > 70% match
                if(KeywordSplitter.stringMatchCount(mQuery, item_text) >= mSearchSet.size()*0.7){
                    mItemSet.add(item);
                }


            }
            exeTimer.stop();

            // save items to JSON
            try {
                PrintWriter json_writer = new PrintWriter("landtop.json", "UTF-8");
                json_writer.println("{\"results\":[");
                Iterator iter = landtopItem.iterator();
                while (iter.hasNext()) {
                    Item item = (Item) iter.next();
                    json_writer.print("{\"name\":\"" + item.getText().replace("\"", "\\\"") + "\", ");
                    json_writer.print("\"price\":" + item.getPrice() + ", ");
                    json_writer.print("\"store\":\"LANDTOP\"" + ", ");
                    json_writer.print("\"url\":\"" + item.getUrl() + "\", ");
                    json_writer.print("\"keywords\":[");
                    ArrayList<String> keywords = new ArrayList<>(KeywordSplitter.splitToKeywords(item.getText()));
                    keywords.add("手機");
                    Iterator ks_iter = keywords.iterator();
                    while (ks_iter.hasNext()) {
                        String ks = (String) ks_iter.next();
                        json_writer.print("\"" + ks + "\"");
                        if (ks_iter.hasNext()) {
                            json_writer.print(",");
                        }
                    }
                    json_writer.print("]}");
                    if (iter.hasNext()) {
                        json_writer.println(",");
                    }
                }
                json_writer.println("]}");
                json_writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(candidates.isEmpty()){
            return;
        }

        int maxMatchedCount = Collections.max(candidates.values());
        for(Map.Entry pair : candidates.entrySet()){
            if(pair.getValue() == maxMatchedCount){
                mItemSet.add((Item)pair.getKey());
            }
        }
    }
}
