package com.pricetag;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

public class RsSearchThread extends Thread{

    private String mQuery;
    private String mStoreId;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "Big5";

    public RsSearchThread(String query, String storeId){
        mQuery = query;
        mStoreId = storeId;
    }

    @Override
    public void run(){
        rsSearch();
    }

    public Set<Item> getItemSet(){
        return mItemSet;
    }

    // search ruten's store website
    private void rsSearch(){
        String queryStrEnc = "";
        try{
            queryStrEnc = URLEncoder.encode(mQuery, M_CHARSET);
        }catch (Exception e){
        }
        String searchUrl = "http://search.ruten.com.tw/search/ulist00.php?s="
                + mStoreId + "&k=" + queryStrEnc + "&c=0";
        // current support eng only, not support chinese (bug)

        try {
            exeTimer.start("rsSearch download web");
            WebThread webThread = new WebThread(searchUrl, M_CHARSET);
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_rs.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("rsSearch parse web");
            Elements elements_text = doc.select("span.item-name");
            Elements elements_price = doc.select("span.item-currency-price");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Rs");
            Debugger.println("elements_text.size() = " + elements_text.size(), "Rs");
            Debugger.println("elements_price.size() = " + elements_price.size(), "Rs");

            exeTimer.start("reSearch match items");
            for (int i=0; i<elements_text.size(); i++) {
                String itemText = elements_text.get(i).text();
                int itemPrice = Integer.parseInt(elements_price.get(i).text().replaceAll(",", ""));
                String itemUrl = elements_text.get(i).parent().select("a").attr("abs:href");
                Item item = new Item("<"+mStoreId+">"+itemText, itemPrice, itemUrl, "");
                mItemSet.add(item);
            }
            exeTimer.stop();

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
