package com.pricetag;

import java.io.*;
import java.util.*;

// This class is for PC only

public class ItemCrawlerTestPC {

    public static void main(String[] args) {

        // ItemCrawler object
        ItemCrawler itemCrawler = new ItemCrawler();

        // Get user input from terminal
        System.out.print("Enter search keywords: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        try{
            input = br.readLine();
        }catch (Exception e){
            e.printStackTrace();
        }

        // parse input query
        System.out.println("Your search contains: " + KeywordSplitter.splitToKeywords(input).toString());

        // ExeTimer, debug use
        ExeTimer exeTimer = new ExeTimer();
        exeTimer.start("itemCrawler.search(input)");

        // run search engine
        Set<Item> itemSet = itemCrawler.search(input);

        exeTimer.stop();

        // if item set is empty
        if(itemSet.isEmpty()){
            System.out.println("Items not found.");
            return;
        }

        // Show the result
        List<Item> itemList = itemCrawler.getSortedItemList();
        System.out.println("======================================================================");
        for (Item item : itemList) {
            System.out.println(item);
            Debugger.printlnToFile(item);
        }

        // Clusters process
        /*System.out.println("======================================================================");
        Clusters clusters = new Clusters(itemList);
        clusters.gen();*/

        Debugger.closeFile();
    }
}
