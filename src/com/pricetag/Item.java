package com.pricetag;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class Item implements Comparable<Item>{
    private String text;
    private int price;
    private String url;
    private String imgUrl;

    public Item(String name, int price, String url, String imgUrl){
        this.text = name;
        this.price = price;
        this.url = url;
        this.imgUrl = imgUrl;
    }

    public Set<String> match(Set<String> searchWords){
        Set<String> matchedWords = new HashSet<>();
        String itemLowerCase = text.toLowerCase(Locale.ENGLISH);
        for(String searchStr : searchWords){
            if(itemLowerCase.contains(searchStr)){
                matchedWords.add(searchStr);
            }
        }
        return matchedWords;
    }

    Pattern pricePattern = Pattern.compile("\\$ *(\\d+,)*\\b\\d*");
    public int getPrice() {
        if (price == 0){
            Matcher matcher = pricePattern.matcher(text);
            try {
                if (matcher.find()) {
                    price = Integer.parseInt(matcher.group().substring(1));
                }
            } catch (NumberFormatException e) {
                price = 0;
            }
        }
        else if (price == 1){  // Landtop
            Matcher matcher = pricePattern.matcher(text);
            try {
                if (matcher.find()) {
                    price = Integer.parseInt(matcher.group().substring(1));
                }
            } catch (NumberFormatException e) {
                price = 0;
            }
        }
        return price;
    }

    public String getText(){
        return text;
    }

    public String getUrl() { return url; }

    @Override
    public String toString(){
        if(text.equals(""))
            return "Empty Item";
        return "$ " + String.valueOf(price) + ", \"" + text + "\" " + url;
    }

    @Override
    public int compareTo(Item other){
        if(this.price == other.price){
            return this.text.compareTo(other.text);
        }
        return this.price - other.price;
    }
}
