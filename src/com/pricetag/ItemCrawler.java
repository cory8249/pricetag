package com.pricetag;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class ItemCrawler {

    private String mQuery;
    private Set<String> mKeywordSet = new HashSet<>();
    private Set<Item> mItemSet = new HashSet<>();

    public Set<Item> search(String query) {
        mQuery = query;
        if (mQuery == null || mQuery.isEmpty()) {
            return mItemSet; // empty item set
        }

        // parse entire string to set of keywords
        mKeywordSet = KeywordSplitter.splitToKeywords(mQuery);

        // search thread
        CoolpcSearchThread coolpcSearchThread = new CoolpcSearchThread(mQuery);
        RutenSearchThread rutenSearchThread = new RutenSearchThread(mQuery);
        RsSearchThread rsSearchThread = new RsSearchThread(mQuery, "pchome");
        LandtopSearchThread landtopSearchThread = new LandtopSearchThread(mQuery);


        // threads start
        coolpcSearchThread.start();
        rutenSearchThread.start();
        rsSearchThread.start();
        landtopSearchThread.start();

        // collect items from different thread
        try {
            coolpcSearchThread.join();
            rutenSearchThread.join();
            rsSearchThread.join();
            landtopSearchThread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // merge into one set
        mItemSet.addAll(coolpcSearchThread.getItemSet());
        mItemSet.addAll(rutenSearchThread.getItemSet());
        mItemSet.addAll(rsSearchThread.getItemSet());
        mItemSet.addAll(landtopSearchThread.getItemSet());

        Debugger.println("========== There are " + mItemSet.size() + " items found. ==========" );
        Debugger.println("coolpc items = " + String.valueOf(coolpcSearchThread.getItemSet().size())
                , "ItemCrawler");
        Debugger.println("ruten items = " + String.valueOf(rutenSearchThread.getItemSet().size())
                , "ItemCrawler");
        Debugger.println("rs items = " + String.valueOf(rsSearchThread.getItemSet().size())
                , "ItemCrawler");
        Debugger.println("landtop items = " + String.valueOf(landtopSearchThread.getItemSet().size())
                , "ItemCrawler");

        return mItemSet;
    }

    public List<Item> getSortedItemList(){
        List<Item> itemList = new ArrayList<>();
        itemList.addAll(mItemSet);
        Collections.sort(itemList); // sort by price, ascending order
        return itemList;
    }
}
