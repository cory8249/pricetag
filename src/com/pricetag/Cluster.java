package com.pricetag;

import java.util.ArrayList;
import java.util.List;


public class Cluster {
    private List<Integer> members;
    private Integer centroid;
    private int size;
    private double WGSS;

    //constructors
    public Cluster(Integer member)
    {
        this.centroid = member;
        this.members = new ArrayList<>();
        this.members.add(member);
        this.size = 1;
        this.WGSS = 0;
    }
    public Cluster(List<Integer> members)
    {
        this.members = members;
        update();
    }

    //Getters
    public List<Integer> getMembers(){ return this.members; }
    public int getCentroid(){ return this.centroid; }
    public int getSize(){ return this.size; }
    public double getWGSS(){ return this.WGSS; }

    //Insert new item(s) into this cluster
    public void insert(Integer member)
    {
        members.add(member);
        update();
    }
    public void insert(List<Integer> insertList)
    {
        this.members.addAll(insertList);
        update();
    }

    //get the mean of a list of prices
    private Integer calMean(List<Integer> numbers)
    {
        int sum=0;
        for(Integer number : numbers){
            sum += number;
        }
        return sum/numbers.size();
    }

    //check if all the variables are coherent
    private void update()
    {
        this.centroid = this.calMean(this.members);
        this.size = this.members.size();
        this.WGSS = 0;
        for(Integer member : members)
        {
            WGSS += ((member-this.centroid)*(member-this.centroid));
        }
    }


    @Override
    public String toString()
    {
        String clusterToString = new String("[ ");
        for(Integer member : members)
        {
            clusterToString = clusterToString + member + " ";
        }
        clusterToString = clusterToString + "]";

        return clusterToString;
    }
}
