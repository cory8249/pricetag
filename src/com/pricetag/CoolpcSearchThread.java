package com.pricetag;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.PrintWriter;
import java.util.*;

public class CoolpcSearchThread extends Thread {

    private String mQuery;
    private Set<String> mSearchSet;
    private Set<Item> mItemSet = new HashSet<>();
    private ExeTimer exeTimer = new ExeTimer();
    private final String M_CHARSET = "Big5";

    public CoolpcSearchThread(String query) {
        mQuery = query;
        mSearchSet = KeywordSplitter.splitToKeywords(mQuery);
    }

    public Set<Item> getItemSet() {
        return mItemSet;
    }

    @Override
    public void run() {
        coolpcSearch();
    }

    // search from coolpc website
    private void coolpcSearch() {

        String coolpcUrl = "http://www.coolpc.com.tw/evaluate.php";
        Map<Item, Integer> candidates = new HashMap<>();

        ArrayList<Item> coolpcItem = new ArrayList<>();

        try {
            exeTimer.start("coolpc download web");
            WebThread webThread = new WebThread(coolpcUrl, M_CHARSET);
            webThread.useCache(true);
            webThread.setCacheExpireTimeMs(60 * 60 * 1000L); // expiration time: 1 hour
            webThread.start();
            webThread.join();
            webThread.dumpToFile(Config.STORAGE_PATH + "debug_coolpc.html");
            Document doc = webThread.getDoc();
            exeTimer.stop();

            exeTimer.start("coolpc parse web");
            Elements elements = doc.select("OPTION");
            exeTimer.stop();
            Debugger.println("downloadSizeByte = " + webThread.htmlSize() / 1024 + " KB", "Coolpc");
            Debugger.println("elementsCount = " + elements.size(), "Coolpc");

            exeTimer.start("coolpc match items");
            for (Element elem : elements) {
                Item item = new Item(elem.text(), 0, "", "");
                int price = item.getPrice();

                // ignore garbage data
                if (price <= 1) {
                    continue;
                }
                coolpcItem.add(item);
                int matchedCount = item.match(mSearchSet).size(); // matching search set
                if (matchedCount > mSearchSet.size() / 2) { // at least match half of keywords
                    candidates.put(item, matchedCount);
                }
            }
            exeTimer.stop();

            // save items to JSON
            try {
                PrintWriter json_writer = new PrintWriter("coolpc.json", "UTF-8");
                json_writer.println("{\"results\":[");
                Iterator iter = coolpcItem.iterator();
                while (iter.hasNext()) {
                    Item item = (Item) iter.next();
                    json_writer.print("{\"name\":\"" + item.getText().replace("\"", "\\\"") + "\", ");
                    json_writer.print("\"price\":" + item.getPrice() + ", ");
                    json_writer.print("\"keywords\":[");
                    ArrayList<String> keywords = new ArrayList<>(KeywordSplitter.splitToKeywords(item.getText()));
                    Iterator ks_iter = keywords.iterator();
                    while (ks_iter.hasNext()) {
                        String ks = (String) ks_iter.next();
                        json_writer.print("\"" + ks + "\"");
                        if (ks_iter.hasNext()) {
                            json_writer.print(",");
                        }
                    }
                    json_writer.print("]}");
                    if (iter.hasNext()) {
                        json_writer.println(",");
                    }
                }
                json_writer.println("]}");
                json_writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (candidates.isEmpty()) {
            return;
        }

        int maxMatchedCount = Collections.max(candidates.values());
        for (Map.Entry pair : candidates.entrySet()) {
            if (pair.getValue() == maxMatchedCount) {
                mItemSet.add((Item) pair.getKey());
            }
        }
    }
}
